
<?php get_template_part('templates/content','cta') ?>
<?php get_template_part('templates/content','partners') ?>
<footer class="footer">
  <div class="container">
    <div class="footer__quote">
      <blockquote>
        <p><?php echo get_field('quote','option')['quote'] ?></p>
        <cite><?php echo get_field('quote','option')['cite'] ?></cite>
      </blockquote>
    </div>

    <div class="footer__container">
        <div class="footer__col">
          <a href="<?php echo site_url()?>" class="footer__logo"><img  src='<?php echo get_template_directory_uri()?>/assets/images/aletheea.svg'></a>
          <p><?php the_field('footer_info','option')?></p>
        </div>
        <div class="footer__col">
          <h3><?php _e('Site Map','yass') ?></h3>
          <?php
                wp_nav_menu( array(
                    'theme_location' => 'site',
                    'link_before'    => '<span>',
                    'link_after' => '</span>',
                    'container'=> false,
                ) );
            ?>
        </div>
        <div class="footer__col">
          <h3><?php _e('Despre Aletheea','yass') ?></h3>
          <?php
                wp_nav_menu( array(
                    'theme_location' => 'about',
                    'link_before'    => '<span>',
                    'link_after' => '</span>',
                    'container'=> false,
                ) );
            ?>
        </div>
        <div class="footer__col footer__col">
          <h3><?php _e('Newsletter','yass') ?></h3>
          <form>
            <input type="email" placeholder="Adresa de email">
            <input type="submit" value="Trimite">
          </form>
          <h3><?php _e('Online','yass') ?></h3>
          <ul>
            <li><a href="<?php the_field('facebook','option')?>" target="_blank" rel="noreferrer">Facebook</a></li>
            <li><a href="<?php the_field('instagram','option')?>" target="_blank" rel="noreferrer">Instagram</a></li>
            <li><a href="<?php the_field('linedIn','option')?>" target="_blank" rel="noreferrer">LinedIn</a></li>
          </ul>
        </div>
    </div>


      <div class="copy"><?php echo date('Y') ?> © Aletheea - Scoala Privata. Toate Drepturile Rezervate. Proudly Crafted by <a href="http://quart.ro" target="_blank" rel="noreferrer">Quart Agency</a>.</div>
  
    </div>
</footer>

<?php get_template_part('templates/content','menu') ?>
<?php get_template_part('templates/content','svg') ?>
<?php wp_footer(); ?>
</body>
</html>