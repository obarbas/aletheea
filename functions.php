<?php

if (!function_exists('yass_setup')) :

    function yass_setup()
    {
        load_theme_textdomain('yass', get_template_directory() . '/languages');

        //Let WordPress manage the document title.
        add_theme_support('title-tag');

        //Enable support for Post Thumbnails on posts and pages.
        add_theme_support('post-thumbnails');

        set_post_thumbnail_size(1200, 9999);
        register_nav_menus(array(
            'main' => __('Main','yass'),
            'site' => __('Site Map','yass'),
            'about' => __('About','yass'),

            'general' => __('General','yass'),
            'about2' => __('About us','yass'),
            'curriculum' => __('Curriculum','yass'),
            'viata' => __('Viata la aletheea','yass'),
            'ateliere' => __('Ateliere','yass'),
            'responsibility' => __('Responsibility','yass'),
            'terms' => __('Terms Mobile','yass'),
        ));
        //  HTML5 support.

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));


    }
endif;
add_action('after_setup_theme', 'yass_setup');

function yass_scripts(){

    wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css', array());
    wp_enqueue_script('main.js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '', true);

}
add_action('wp_enqueue_scripts', 'yass_scripts');

show_admin_bar(false);

require_once 'inc/woo.php';

/* Options Frame Work End Here */

class Add_button_of_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent <svg   class='toggle-btn'><use xlink:href='#menu'></use></svg><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
}


add_action( 'init', 'cariere' );
function cariere() {
    register_post_type( 'cariera',
        array(
            'labels' => array(
                'name' => __( 'Cariere' ),
                'singular_name' => __( 'Cariere' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-clipboard',
            'supports' => array('title','editor','thumbnail' ),

        )
    );
};


add_action( 'init', 'profesori' );
function profesori() {
    register_post_type( 'profesor',
        array(
            'labels' => array(
                'name' => __( 'Profesori' ),
                'singular_name' => __( 'Profesori' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-businessperson',
            'supports' => array('title', 'thumbnail' ),

        )
    );
};


add_action( 'init', 'cluburi' );
function cluburi() {
    register_post_type( 'club',
        array(
            'labels' => array(
                'name' => __( 'Cluburi' ),
                'singular_name' => __( 'Club' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-businessperson',
            'supports' => array('title', 'thumbnail' ),

        )
    );
};

add_action( 'init', 'parteneri' );
function parteneri() {
    register_post_type( 'partener',
        array(
            'labels' => array(
                'name' => __( 'Parteneri' ),
                'singular_name' => __( 'Partener' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-businessperson',
            'supports' => array('title', 'thumbnail' ),

        )
    );
};


add_action( 'init', 'galeries' );
function galeries() {
    register_post_type( 'gallery',
        array(
            'labels' => array(
                'name' => __( 'Galleries' ),
                'singular_name' => __( 'Gallery' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-businessperson',
            'supports' => array('title', 'thumbnail' ),

        )
    );
};

add_action( 'init', 'ateliere' );
function ateliere() {
    register_post_type( 'atelier',
        array(
            'labels' => array(
                'name' => __( 'Ateliere' ),
                'singular_name' => __( 'Ateliere' )
            ),
            'has_archive' => false,
            'public' => true,
            'publicly_queryable'  => true,
            'show_in_rest'=> false,
            'menu_icon' => 'dashicons-businessperson',
            'supports' => array('title', 'thumbnail' ),

        )
    );
};

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Cariere',
		'menu_title'	=> 'Cariere',
		'parent_slug'	=> 'theme-settings',
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Profesori',
		'menu_title'	=> 'Profesori',
		'parent_slug'	=> 'theme-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Cluburi',
		'menu_title'	=> 'Cluburi',
		'parent_slug'	=> 'theme-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Blog',
		'menu_title'	=> 'Blog',
		'parent_slug'	=> 'theme-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Partneri',
		'menu_title'	=> 'Partneri',
		'parent_slug'	=> 'theme-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Galleries',
		'menu_title'	=> 'Galleries',
		'parent_slug'	=> 'theme-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Shop',
		'menu_title'	=> 'Shop',
		'parent_slug'	=> 'theme-settings',
	));
}

// Register Custom Taxonomy
function custom_taxonomy_club() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Categories', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
        'show_in_rest'=> true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'club-category', array( 'club' ), $args );

}
add_action( 'init', 'custom_taxonomy_club', 0 );


// Register Custom Taxonomy
function custom_taxonomy_profesor() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Categories', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
        'show_in_rest'=> true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'profesor-category', array( 'profesor' ), $args );

}
add_action( 'init', 'custom_taxonomy_profesor', 0 );




add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	
	// loop
	foreach( $items as &$item ) {
		
		// vars
        $icon = get_field('icon', $item);

		// append icon
		if( $icon ) {
            $item->title .=  '</span>' ;
			$item->title .=  wp_get_attachment_image( $icon, 'full','',array('class'=>'') ) ;
		}
		
	}
	
	// return
	return $items;
	
}