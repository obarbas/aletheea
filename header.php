<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kalam&display=swap" rel="stylesheet">
    <?php wp_head();  ?>

</head>
<body <?php body_class()?> >

<header class="header">
    <div class="header__left">
        <div class="header__nav">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'main',
                    'link_before'    => '<span>',
                    'link_after' => '</span>',
                    'container'=> false,
                    'walker'         => new Add_button_of_Sublevel_Walker
                ) );
            ?>
        </div>
    
    </div>
    <a href="<?php echo site_url()?>" class="header__logo"><img  src='<?php echo get_template_directory_uri()?>/assets/images/aletheea.svg'></a>
    <div class="header__right">
        <a href="#" class="btn btn--border">Ateliere Aletheea <svg width="6.813" height="7.57"><use xlink:href="#next"></use></svg></a>
        <a href="#" class="btn btn--green">Inscriere <svg width="6.813" height="7.57"><use xlink:href="#next"></use></svg></a>

        <a class="header__link" href="#" target="_blank" rel="noreferrer"><svg width="8.005" height="14.588"><use xlink:href="#facebook"></use></svg></a>
        <a class="header__link" href="#"><svg width="14.213" height="14.212"><use xlink:href="#search"></use></svg></a>
        <a class="header__link" href="#"><svg width="15.524" height="15.522"><use xlink:href="#call"></use></svg></a>
        <button class="burger">
            <span class="burger__icon">
                <span></span>
                <span></span>
                <span></span>
            </span>
            <span>Menu</span>
        </button>
    </div>

</header>

