<?php get_header(); ?>	

<section class="page-header">
  <?php $image = get_field('image_phb', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_phb', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_phb', 'option')?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>
<?php get_template_part('templates/content','sections-viata') ?>

<section class="articles">

  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle_bt', 'option')?></span>
      <h2 class="title title--magenta"><?php the_field('title_bt', 'option')?></h2>
    </div>

    <div class="articles__grid">
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php get_template_part('templates/content','post') ?> 
      <?php endwhile;  endif; ?>
    </div>


  </div>

</section>



<?php get_footer(); ?> 