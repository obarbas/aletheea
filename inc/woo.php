<?php 

//Declare WooCommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-slider' );
}


//WC theme compatibility

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
// * remove woocommerce_breadcrumb - 20

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',20 );

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<div class="woo-container">';
}

function my_theme_wrapper_end() {
    echo '</div> ';
}


// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'yass_dequeue_styles' );
function yass_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}


// Remove Woocommerce Select2 

function woo_dequeue_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );
    } 
}
add_action( 'wp_enqueue_scripts', 'woo_dequeue_select2', 100 );

// Remove # of products "showing X product x" and ordering
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//move sale badge

remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
add_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 4);

//remove add to cart button 

remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart', 10);

// Add the wrap to products
add_action( 'woocommerce_before_shop_loop_item_title', create_function('', 'echo "<span class=\"image-wrap\">";'), 5, 2);
add_action( 'woocommerce_before_shop_loop_item_title', create_function('', 'echo "</span>";'), 12, 2);

// If there is sub categories add wrap too
add_action( 'woocommerce_before_subcategory_title', create_function( '', 'echo "<div class=\"image-wrap\">";'), 5, 2 );
add_action( 'woocommerce_before_subcategory_title', create_function( '', 'echo "</div>";' ), 12, 2 );

//Remove title hook and add in a new one with the product categories added
add_action( 'woocommerce_shop_loop_item_title', 'woo_loop_product_cat', 4 );

function woo_loop_product_cat() {
    $terms = get_the_terms( $post->ID, 'product_cat' );
    if ( $terms && ! is_wp_error( $terms ) ) :
        $cat_links = array();
        foreach ( $terms as $term ) {
            $cat_links[] = $term->name;
        }
        ?>
        <span class="product__category">
            <?php echo  $cat_links[0]; ?>
        </span>
    <?php endif;
}



remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary','woocommerce_template_single_meta', 4);

function addDescription(){
    ?>
    <div class="product__description"><?php the_content()?></div>
    <?php
}
add_action('woocommerce_single_product_summary','addDescription', 20);

/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
    global $product;
      $args['posts_per_page'] = 3;
      return $args;
  }
  add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
    function jk_related_products_args( $args ) {
      $args['posts_per_page'] = 3; // 4 related products
      $args['columns'] = 3; // arranged in 2 columns
      return $args;
  }

//Remove downloads from menu in my account page
function custom_my_account_menu_items( $items ) {
    unset($items['downloads']);
    return $items;
}
add_filter( 'woocommerce_account_menu_items', 'custom_my_account_menu_items' );



//remove tabs

add_filter( 'woocommerce_product_tabs', '__return_empty_array', 98 );

/**
 * Disable reviews.
 */
function iconic_disable_reviews() {
	remove_post_type_support( 'product', 'comments' );
}

add_action( 'init', 'iconic_disable_reviews' );