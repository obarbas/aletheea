<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>


<section class="default">
  <div class="container">
    <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title"><?php if( get_field('title') ){ echo get_the_field('title'); }else{ echo get_the_title(); }?></h2>
        <p><?php the_field('info')?></p>
      </div>
      <div class="the-content">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
     <?php endwhile; endif; ?>


      </div>
  </div>
</section>
 
<?php get_footer(); ?> 