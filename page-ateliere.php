<?php /*Template Name: Ateliere*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-ateliere') ?>
<section class="ateliere">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--purple"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>
      <div class="ateliere__top"><?php the_field('content_top')?></div>


      <div class="ateliere__grid">
        <?php  $args = array(
              'post_type' => 'atelier',
              'posts_per_page'=> -1,
              'orderby'=> 'date',
              'order' => 'DESC',
          );
          $loop = new WP_Query( $args );
          if ( $loop->have_posts() ) : ?>
              <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','atelier') ?>
              <?php endwhile;?>
          <?php else: endif; wp_reset_postdata(); ?>
      </div>



  </div>

 
</section>


<?php get_footer(); ?> 