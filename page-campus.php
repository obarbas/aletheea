<?php /*Template Name: Campus*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-viata') ?>
 





<section class="campus">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title"><?php the_field('title')?></h2>
      <p> <?php the_field('info')?></p>
    </div>
  </div>
  <div class="campus__grid">
   
    <?php if( have_rows('photo_grid') ): while ( have_rows('photo_grid') ) : the_row(); ?>
     
    <div class="campus__col campus__col--<?php echo get_row_index()?>">
    <?php $images = get_sub_field('pictures');
      $size = 'full'; // (thumbnail, medium, large, full or custom size)
    if( $images ): ?>
        <?php foreach( $images as $image ): ?>
          <?php echo wp_get_attachment_image( $image, $size ); ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
     
    <?php endwhile; endif; ?>
    
  </div>

  <div class="campus__bottom">
    <h4 class="title title--white"><?php the_field('text_bottom')?></h4>
  </div>
</section>


<section class="camp">
  <div class="container">
      
    <div class="col2__grid">
        <div class="col2__col col2__col--1">
          <span><?php the_field('subtitle_c')?></span>
          <h4><?php the_field('title_c')?></h4>
        </div>
        <div class="col2__col col2__col--2">
          <?php the_field('info_c')?>
        </div>
    </div>
    <div class="story__banner"><?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?></div>


    <div class="dotari">
      <h2 class="title title--magenta"><?php the_field('title_b')?></h2>
      <div class="dotari__info">  <?php the_field('info_b')?></div>
    </div>

    <div class="dotari__grid">
      <h4><?php the_field('title_f')?></h4>
      <div class="dotari__grid__grid">
        <?php if( have_rows('features') ): while ( have_rows('features') ) : the_row(); ?>
         
          <div class="dotari__col">
            <?php $image = get_sub_field('icon'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
            <span><?php the_sub_field('item')?></span>
          </div>
         
        <?php endwhile; endif; ?>
      </div>
    </div>



  </div>
</section>

<?php get_footer(); ?> 