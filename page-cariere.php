<?php /*Template Name: Cariere*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections') ?>

<section class="cariere">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--blue"><?php the_field('title')?></h2>
      <?php the_field('info')?>
    </div>

    <div class="cariere__grid">
        <?php  $args = array(
            'post_type' => 'cariera',
            'posts_per_page'=> -1,
            'orderby'=> 'date',
            'order' => 'DESC',
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','cariera') ?>
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
    </div>

    
  </div>
</section>



<?php get_footer(); ?> 