<?php /*Template Name: Charitable*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-responsibility') ?>
 
<section class="charitable">
  <div class="container">
    <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--red"><?php the_field('title')?></h2>
        <p><?php the_field('info')?></p>
    </div>

    <div class="charitable__grid">
      <div class="charitable__col charitable__col--1">
        <h4><?php the_field('title2')?></h4>
        <?php the_field('info2')?>
      </div>
      <div class="charitable__col charitable__col--2">
        <?php $image = get_field('image1'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
      </div>
      <div class="charitable__col charitable__col--3">
        <?php $image = get_field('image2'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
      </div>
      <div class="charitable__col charitable__col--4">
        <blockquote class="philosophy__quote">
            <p><?php echo get_field('quote')['quote'] ?></p>
            <cite><?php echo get_field('quote')['cite'] ?></cite>
        </blockquote>
      </div>
    </div>

    <div class="charitable__gallery">
      <h3 class="title title--red"><?php the_field('title3')?></h3>
    </div>

    <div class="small-gallery">
      <?php $images = get_field('gallery');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
      if( $images ): ?>
         <?php foreach( $images as $image ): ?>
          <a class="glightbox" href="<?php echo wp_get_attachment_url($image); ?>">
          <?php echo wp_get_attachment_image( $image, $size,'', array('class'=>'img-abs') ); ?>
          </a>
         <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</section>
 
<?php get_footer(); ?> 