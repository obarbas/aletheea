<?php /*Template Name: Aletheea in Cifre*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-about') ?>
 

<section class="cifre">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--orange"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>
    <div class="cifre__grid">
      <?php if( have_rows('numbers') ): while ( have_rows('numbers') ) : the_row(); ?>
       
      <div class="cifre__item" style="background-color:<?php the_sub_field('color')?>">
          <span class="cifre__top">
            <span class="subtitle"><?php the_sub_field('subtitle')?></span>
            <h4 class="title"><?php the_sub_field('title')?></h4>
          </span>
          <?php the_sub_field('info')?>
        </div>  
       
      <?php endwhile; endif; ?>

    </div>
    <div class="cifre__bottom">
      <h3 class="title"><?php the_field('title2')?></h3>
    </div>
    <div class="story__banner">
        <?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
    </div>
  </div>
</section>


 
<?php get_footer(); ?> 