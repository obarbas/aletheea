<?php /*Template Name: Cluburi*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-curriculum') ?>


<section class="cluburi">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--green"><?php the_field('title')?></h2>
        <p> <?php the_field('info')?></p>
      </div>

      <div class="cluburi__offers">
        <?php if( have_rows('offersx') ): while ( have_rows('offersx') ) : the_row(); ?>
         
          <a href="<?php the_sub_field('file')?>" target="_blank">
            <?php $image = get_sub_field('icon'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
            <span><?php the_sub_field('title')?></span>
          </a>
         
        <?php endwhile; endif; ?>
      </div>
    </div>
</section>
<div class="cluburi__terms">
  <?php 
  $terms = get_terms( 'club-category', array(
      'hide_empty' => false,
  ) ); 
  foreach($terms as $term):
?>
  <a href="#"><?php echo $term->name ?></a>
<?php endforeach ?>
</div>

<section class="cluburi2">
  <div class="container">
    <div class="cluburi__grid">
    <?php  $args = array(
            'post_type' => 'club',
            'posts_per_page'=> -1,
            'orderby'=> 'date',
            'order' => 'DESC',
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','club') ?>
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
    </div>
  </div>
</section>
 
<?php get_footer(); ?> 