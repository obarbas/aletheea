<?php /*Template Name: Contact*/ ?>
<?php get_header(); ?>	

<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections') ?>


<section class="contact">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--blue"><?php the_field('title')?></h2>
      <?php the_field('info')?>
    </div>

    <div class="contact__grid">
        <?php if( have_rows('boxes') ): while ( have_rows('boxes') ) : the_row(); ?>
         
        <div class="contact__col">
          <?php $image = get_sub_field('icon'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
          <div class="contact__info">
            <h4><?php the_sub_field('title')?></h4>
            <?php the_sub_field('info')?>
          </div>
        </div>
         
        <?php endwhile; endif; ?>
        
    </div>


    <div class="contact__map">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle_2')?></span>
        <h2 class="title title--green"><?php the_field('title_2')?></h2>
      </div>
      
      <div id="map"><?php the_field('iframe')?></div>

    </div>


  </div>
</section>


<?php get_footer(); ?> 