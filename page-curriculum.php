<?php /*Template Name: Curriculum*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-curriculum') ?>

<section class="curriculum">
  <div class="container">
    
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--green"><?php the_field('title')?></h2>
      <p><?php the_field('info')?></p>
    </div>

    <div class="curriculum__top">
      <?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
       
      <div class="curriculum__col">
        <span><?php the_sub_field('subtitle')?></span>
        <h4><?php the_sub_field('title')?></h4>
        <?php the_sub_field('info')?>
      </div>
       
      <?php endwhile; endif; ?>
    </div>

  </div>
</section>

  <div class="orange-line">
    <?php $image = get_field('banner2'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  </div>

<section class="curriculum curriculum--2">

  <div class="container">

    <div class="col2__grid">
      <div class="col2__col col2__col--1">
        <span><?php the_field('subtitle_is')?></span>
        <h4><?php the_field('title_is')?>></h4>
      </div>
      <div class="col2__col col2__col--2">
        <?php the_field('info_is')?>
      </div>
    </div>


    <div class="curriculum__banner">
      <h3 class="title"><?php the_field('title_b')?></h3>
      <?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
    </div>
    <div class="curriculum__bottom">
      <h3 class="title title--green"><?php the_field('title_ib')?></h3>
      <div class="curriculum__bottom__info">
        <?php the_field('info_ib')?>
      </div>
    </div>
  </div>
</section>
 
<?php get_footer(); ?> 