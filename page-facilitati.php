<?php /*Template Name: Facilitati*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-viata') ?>
 
<section class="facilities">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--magenta"><?php the_field('title')?></h2>
      <p><?php the_field('info')?></p>
    </div>

    <div class="facilities__list">

    
    <?php if( have_rows('facilities') ): while ( have_rows('facilities') ) : the_row(); ?>
     
      <div class="facilities__row">
        <div class="facilities__col facilities__col--1">
          <span><?php the_sub_field('subtitle')?></span>
          <h3 class="title"><?php the_sub_field('title')?></h3>
        </div>
        <div class="facilities__col facilities__col--2">
          <div class="facilities__banner">
            <?php $image = get_sub_field('image'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
          </div>
          <?php the_sub_field('info')?>
        </div>
      
        
      </div>
     
    <?php endwhile; endif; ?>


    </div>


  </div>
</section>
 
<?php get_footer(); ?> 