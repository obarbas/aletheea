<?php /*Template Name: Filozofie*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-about') ?>
 

<section class="filozofie">
  <div class="container">
    
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--orange"><?php the_field('title')?></h2>
      <?php the_field('info')?>
    </div>
    <div class="filozofie__info">
      <h3 class="title title--green"><?php _e('Misiune','yass') ?></h3>
        <?php the_field('info_2')?>
    </div>
    <div class="filozofie__boxes">
      <div class="filozofie__box">
        <h4 class="title title--white"><?php _e('Viziune','yass') ?></h4>
        <?php the_field('info_3')?>
      </div>
      <div class="filozofie__box">
        <h4 class="title title--white"><?php _e('Valori','yass') ?></h4>
        <?php the_field('info_4')?>
      </div>
    </div>
  </div>
</section>
<section class="filozofie2">
    <div class="container">
      <div class="main-title">
        <h2 class="title title--orange"><?php the_field('title')?></h2>
        <p><?php the_field('info_3')?></p>
      </div>

      <div class="filozofie2__grid">
          <?php if( have_rows('principles') ): while ( have_rows('principles') ) : the_row(); ?>
           
          <div class="filozofie2__col">
              <div class="filozofie2__col__inner">
                <div class="filozofie2__info">
                  <?php $image = get_sub_field('icon'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
                  <h4><?php the_sub_field('title')?></h4>
                  <?php the_sub_field('info')?>
                </div>
              </div>
                
            </div>
           
          <?php endwhile; endif; ?>
        
      </div>
    </div>
</section>

<?php get_footer(); ?> 