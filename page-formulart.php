<?php /*Template Name: Formular Inscriere*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-curriculum') ?>


<section class="formular">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--green"><?php the_field('title')?></h2>
        <p> <?php the_field('info')?></p>
      </div>


      <?php echo do_shortcode('[contact-form-7 id="'.get_field('form').'"]') ?>
    </div>
</section>


<?php get_footer(); ?> 