<?php /*Template Name: Good Practices*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-responsibility') ?>
 
<section class="practices">
  <div class="container">
      <div class="main-title">
          <span class="subtitle"><?php the_field('subtitle')?></span>
          <h2 class="title title--red"><?php the_field('title')?></h2>
          <p><?php the_field('info')?></p>
      </div>

      <div class="practices__grid">
        <?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
         
        <div class="practices__row">
          <div class="practices__info">
            <h4><?php the_sub_field('title')?></h4>
            <?php the_sub_field('info')?>
          </div>
          <div class="practices__img">

          <div class="practices__gallery swiper-container">
            <div class="swiper-wrapper">
              <?php $images = get_sub_field('gallery');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)
              if( $images ): ?>
                 <?php foreach( $images as $image ): ?>
                  <a class="swiper-slide" href="<?php echo wp_get_attachment_url($image); ?>">
                  <?php echo wp_get_attachment_image( $image, $size, '', array('class'=>'img-abs') ); ?>
                  </a>
                 <?php endforeach; ?>
              <?php endif; ?>
            </div>
            <div class="swiper-pagination"></div>
          </div>
          </div>
        </div>
         
        <?php endwhile; endif; ?>
      </div>



  </div>
</section>
 
<?php get_footer(); ?> 