<?php /*Template Name: Homepage*/ ?>
<?php get_header(); ?>	
 
<section class="hero">
  <div class="hero__slider swiper-container">
    <div class="swiper-wrapper">
    <?php if( have_rows('slides') ): while ( have_rows('slides') ) : the_row(); ?>
     
    <div class="swiper-slide hero__slide" data-title="<?php the_sub_field('slide_title')?>">
        <?php $image = get_sub_field('image_desktop'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
        <div class="container">
          <div class="hero__info">
            <h2 class="hero__title"><?php echo get_sub_field('info')['title'] ?></h2>
            <p><?php echo get_sub_field('info')['info'] ?></p>
            <a href="<?php echo get_sub_field('info')['button']['label'] ?>" class="btn btn--orange"><?php echo get_sub_field('info')['button']['label'] ?></a>
          </div>
        </div>
      </div>
     
    <?php endwhile; endif; ?>
    </div>
   
  </div>
  <div class="hero__controls container">
    <div class="hero__pagination"></div>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>

<section class="philosophy">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php echo get_field('main_title')['subtitle'] ?></span>
      <h2 class="title"><?php echo get_field('main_title')['title'] ?></h2>
    </div>

    <div class="philosophy__info">
    <?php the_field('info')?>
    <a href="<?php echo get_field('button')['url'] ?>" class="btn btn--orange"><?php echo get_field('button')['label'] ?></a>

      <blockquote class="philosophy__quote">
          <p><?php echo get_field('quote')['quote'] ?></p>
          <cite><?php echo get_field('quote')['cite'] ?></cite>
      </blockquote>
    </div>
  </div>

</section>
 

<section class="campus">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php echo get_field('main_title_c')['subtitle'] ?></span>
      <h2 class="title"><?php echo get_field('main_title_c')['title'] ?></h2>
      <p><?php echo get_field('main_title_c')['info'] ?></p>
    </div>
  </div>
  <div class="campus__grid">
   
    <?php if( have_rows('photo_grid') ): while ( have_rows('photo_grid') ) : the_row(); ?>
     
    <div class="campus__col campus__col--<?php echo get_row_index()?>">
    <?php $images = get_sub_field('pictures');
      $size = 'full'; // (thumbnail, medium, large, full or custom size)
    if( $images ): ?>
        <?php foreach( $images as $image ): ?>
          <?php echo wp_get_attachment_image( $image, $size ); ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
     
    <?php endwhile; endif; ?>
    
  </div>

  <div class="campus__bottom">
    <p><?php the_field('text_bottom')?></p>
    <a href="<?php echo get_field('button_c')['url'] ?>" class="btn btn--white"><?php echo get_field('button_c')['label'] ?></a>

  </div>
</section>

<section class="prof">
  <div class="container">
    <div class="main-title">
    <span class="subtitle"><?php echo get_field('main_title_p')['subtitle'] ?></span>
      <h2 class="title"><?php echo get_field('main_title_p')['title'] ?></h2>
    </div>

    <div class="prof__info">
      <div class="prof__img">
        <?php $image = get_field('professor')['image']; if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <h3 class="title"><?php echo get_field('professor')['name'] ?></h3>
        <span><?php echo get_field('professor')['role'] ?></span>
      </div>
      <div class="prof__text">
        <?php the_field('info_p')?>

        <div class="prof__text__btns">
          <?php if( have_rows('buttons') ): while ( have_rows('buttons') ) : the_row(); ?>
          <a href="<?php the_sub_field('url')?>" class="btn <?php if( get_row_index() == 2 ){ echo 'btn--purple';}else { echo 'btn--orange'; }?>"><?php the_sub_field('label')?></a>
          <?php endwhile; endif; ?>
        </div>
        
      </div>
      
    </div>

    <div class="prof__links">
      <?php if( have_rows('links') ): while ( have_rows('links') ) : the_row(); ?>
       
      <div class="prof__link">
          <a href="<?php the_sub_field('url') ?>">
            <?php $image = get_sub_field('image'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
            <h4 class="title"><?php the_sub_field('title')?></h4>
          </a>
      </div>
       
      <?php endwhile; endif; ?>
    </div>

  </div>
</section>


<section class="banner">
  <div class="banner__slider swiper-container">
    <div class="swiper-wrapper">
     
    <?php if( have_rows('banners') ): while ( have_rows('banners') ) : the_row(); ?>
     
      <div class="swiper-slide">
          <?php $image = get_sub_field('image'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
        <div class="container">
          <span class="subtitle"><?php the_sub_field('subtitle')?></span>
          <h3 class="title"><?php the_sub_field('title')?></h3>
        </div>
      </div>
     
    <?php endwhile; endif; ?>
     
      
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</section>


<section class="articles">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php echo get_field('main_title_b')['subtitle'] ?></span>
        <h2 class="title title--green"><?php echo get_field('main_title_b')['title'] ?></h2>
      </div>

      <div class="articles__grid">
      <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page'=> 3,
            'orderby'=> 'date',
            'order' => 'DESC',
        
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','post') ?> 
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
        
          
      </div>
      <div class="center">
        <a href="<?php echo get_the_permalink(get_option('page_for_posts'))?>" class="btn btn--purple"><?php _e('Vezi mai multe noutati','yass') ?></a>
      </div>
  </div>
</section>


<section class="boxlink">
  <?php $image = get_field('image_l2'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
  <?php if( have_rows('links2') ): while ( have_rows('links2') ) : the_row(); ?>
    <a href="<?php the_sub_field('link')?>" class="boxlink__col"><?php the_sub_field('title')?></a>
  <?php endwhile; endif; ?>
  </div>
</section>



<?php get_footer(); ?> 