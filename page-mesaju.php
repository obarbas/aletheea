<?php /*Template Name: Mesajul Directorilor*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-about') ?>

<section class="mesajul">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--orange"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>

  </div>
</section>
<?php if( have_rows('directors') ): while ( have_rows('directors') ) : the_row(); ?>
 
<section class="mesajul mesajul--<?php echo get_row_index()?>">
  <div class="container">
      <div class="story__grid">
        <div class="story__img">
          <?php $image = get_sub_field('director')['image']; if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
          <h4><?php echo get_sub_field('director')['name'] ?></h4>
          <p><?php echo get_sub_field('director')['role'] ?></p>
          </div>
          <div class="story__info">
              <h3 class="title title--green"><?php the_sub_field('title')?></h3>
              <?php the_sub_field('info')?>
            </div>   
      </div>
  </div>
</section>
 
<?php endwhile; endif; ?>
<?php get_footer(); ?> 