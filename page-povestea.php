<?php /*Template Name: Povestea*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-about') ?>

<section class="story">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--orange"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>


      <div class="story__grid">
        <div class="story__img">
          <?php $image = get_field('director')['image']; if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
          <h4><?php echo get_field('director')['name'] ?></h4>
          <p><?php echo get_field('director')['role'] ?></p>
        </div>
        <div class="story__info"><?php the_field('info_1')?></div>
      </div>

      <div class="story__banner">
        <?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
      </div>


      <div class="story__bottom"><?php the_field('info_2')?></div>

  </div>
</section>
<?php get_footer(); ?> 