<?php /*Template Name: Prezentare Video*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-viata') ?>
 
<section class="videoprez">
  <div class="container">
    
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--magenta"><?php the_field('title')?></h2>
      <p><?php the_field('info')?></p>
    </div>
  
    <div class="club-single__videos">
      <?php if( have_rows('videos') ): while ( have_rows('videos') ) : the_row(); ?>
       
      <a href="<?php the_sub_field('video_url')?>" class="club-single__video glightbox">
        <span class="club-single__poster">
          <?php $image = get_sub_field('video_poster'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
          <svg width="55.515" height="55.515"><use xlink:href="#video"></use></svg>
        </span>
        <h4><?php the_sub_field('info')?></h4>
      </a>
       
      <?php endwhile; endif; ?>
    </div>
  </div>
</section>
 
<?php get_footer(); ?> 