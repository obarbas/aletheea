<?php /*Template Name: Prezentare*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-curriculum') ?>


<section class="prezentare">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle')?></span>
      <h2 class="title title--green"><?php the_field('title')?></h2>
      <p><?php the_field('info')?></p>
    </div>

    <div class="col2__grid">
      <div class="col2__col col2__col--1">
        <span><?php echo get_field('content')['subtitle']?></span>
        <h4><?php echo get_field('content')['title']?></h4>
      </div>
      <div class="col2__col col2__col--2">
        <?php echo get_field('content')['info']?>
      </div>
    </div>

    <div class="prezentare__gallery ">
        <div class="main-title">
          <h2 class="title"><?php the_field('title2')?></h2>
        </div>
        <div class="competences">
          <?php if( have_rows('competences') ): while ( have_rows('competences') ) : the_row(); ?>
           
          <div class="competence">
            <span><?php the_sub_field('item')?></span>
          </div>
          <?php endwhile; endif; ?>
        </div>
    </div>
    <div class="prezentare__gallery ">
        <div class="main-title">
          <h2 class="title title--green"><?php the_field('title3')?></h2>
          <p><?php the_field('info3')?></p>
          </div>
      </div>
  </div>

      <div class="prezentare__slider swiper-container">
            <div class="swiper-wrapper">
              <?php $images = get_field('gallery2');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)
              if( $images ): ?>
                 <?php foreach( $images as $image ): ?>
                  <a class="glightbox swiper-slide" data-gallery="glightbox-sg" href="<?php echo wp_get_attachment_url($image); ?>">
                        <?php echo wp_get_attachment_image( $image, $size, '', array('class'=> 'img-abs') ); ?>
                  </a>
                 <?php endforeach; ?>
              <?php endif; ?>
            </div>
      </div>


</section>


 
<?php get_footer(); ?> 