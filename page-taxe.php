<?php /*Template Name: Admitere si taxe*/ ?>
<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-about') ?>
 

<section class="taxe">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--orange"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>

      <div class="taxe__imgs">
        <?php if( have_rows('plans') ): while ( have_rows('plans') ) : the_row(); ?>
          <div class="taxe__img"><?php the_sub_field('plan')?></div>         
        <?php endwhile; endif; ?>
      </div>
      <div class="taxe__info"><?php the_field('content')?></div>
      <div class="story__banner">
          <?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
      </div>
  </div>


    <div class="taxe__includes">
      <div class="container">

        <div class="taxe__includes__col">
          <span><?php the_field('subtitle_b')?></span>
          <h4><?php the_field('title_b')?></h4>
        </div>
        <div class="taxe__includes__col"><?php the_field('info_b')?></div>
      </div>
    </div>

  <div class="container">
      <div class="taxe__steps">
        <h3 class="title title--green"><?php the_field('title_a')?></h3>
        <?php the_field('info_a')?>
        <div class="taxe__steps__grid">
          <?php if( have_rows('steps') ): while ( have_rows('steps') ) : the_row(); ?>
           
          <div class="taxe__steps__col">
              <div class="taxe__steps__col__inner">
                <h4><?php the_sub_field('title')?></h4>
                <p> <?php the_sub_field('subtitle')?> </p>
              </div>
            </div>
           
          <?php endwhile; endif; ?>
        </div>
        <?php the_field('info_bottom')?>
        <div class="center">
          <a href="<?php echo get_field('button')['url'] ?>" class="btn btn--green"><?php echo get_field('button')['label'] ?></a>
        </div>
      </div>
  </div>
</section>
 
<?php get_footer(); ?> 