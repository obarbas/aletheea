<?php get_header(); ?>	
<?php get_template_part('templates/content','hero') ?>
<?php get_template_part('templates/content','sections-ateliere') ?>

 <section class="atelier">
   <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--purple"><?php the_field('title')?></h2>
        <?php the_field('info')?>
      </div>

      <div class="atelier__top">
        <div class="atelier__banner">
          <?php $image = get_field('image_top'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
        </div>
        <div class="atelier__cost">
          <span class="subtitle">COST</span>
          <h3 class="title title--purple">120 lei</h3>
          <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
        </div>
      </div>
     
      <div class="col2__grid">
        <div class="col2__col col2__col--1">
          <span><?php the_field('subtitle_t')?></span>
          <h4><?php the_field('title_t')?></h4>
        </div>
        <div class="col2__col col2__col--2">
          <?php the_field('info_t')?>
        </div>
      </div>

      <div class="story__grid">
        <div class="story__img">
          <?php $image = get_field('image_b')['image']; if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
          <h4><?php echo get_field('image_b')['title']?></h4>
          <p><?php echo get_field('image_b')['info']?></p>
        </div>
        <div class="story__info">
          <h3 class="title title--blue"><?php the_field('title_b')?></h3>
          <?php the_field('info_b')?>  
        </div>
    </div>

   </div>
 </section>
<?php get_footer(); ?> 