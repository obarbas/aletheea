<?php get_header(); ?>	
<?php $archive = get_field('page_archive_cariere','option'); ?>

<section class="page-header">
  <?php $image = get_field('image_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_ph',$archive)?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>
<?php get_template_part('templates/content','sections') ?>

<section class="cariera">

  <div class="container">

    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle',$archive)?></span>
      <h2 class="title title--blue"><?php the_field('title',$archive)?></h2>
      <?php the_field('info',$archive)?>
    </div>
    <div class="cariera__grid">
      <div class="cariera__col cariera__col--1">
        <span><?php _e('Cariere','yass') ?></span>
        <h4><?php the_title() ?> </h4>
      </div>
      <div class="cariera__col cariera__col--2">
        <?php the_content() ?>  
        <a href="mailto:secretariat@aletheea.ro?subject= <?php _e('Application for','yass')?> <?php echo get_the_title(); ?> @ Aletheea" class="btn btn--orange"><?php _e('Aplica la acest job','yass') ?></a>
      </div>
      <div class="cariera__col cariera__col--img">
        <?php the_post_thumbnail('large') ?>
      </div>
    </div>



  </div>
</section>
<section class="cariere">
  <div class="container">
    <div class="main-title">
      <h2 class="title title--blue"><?php the_field('title_mc','option')?></h2>
      <p><?php the_field('info_mc','option')?></p>
    </div>

    <div class="cariere__grid">
      <?php  $args = array(
            'post_type' => 'cariera',
            'posts_per_page'=> 3,
            'orderby'=> 'date',
            'order' => 'DESC',
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','cariera') ?>
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
    </div>

    <div class="center">
      <a href="<?php echo get_the_permalink($archive)?>" class="btn btn--orange"><?php _e('Vezi mai multe noutati','yass') ?></a>
    </div>
  </div>
</section>

<?php get_footer(); ?> 