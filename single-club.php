<?php get_header(); ?>	
<?php $archive = get_field('page_archive_club','option'); ?>

<section class="page-header">
  <?php $image = get_field('image_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_ph',$archive)?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>
<?php get_template_part('templates/content','sections-curriculum') ?>

<section class="club-single">
  <div class="container">

    <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--green"><?php the_field('title')?></h2>
        <p><?php the_field('info')?></p>
    </div>

    <div class="story__banner"><?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?></div>

    <div class="col2__grid">
      <div class="col2__col col2__col--1">
        <span><?php the_field('subtitle_c')?></span>
        <h4><?php the_field('title_c')?></h4>
      </div>
      <div class="col2__col col2__col--2">
      <?php the_field('info_C')?>
      </div>
    </div>

    <div class="club-single__videos">
      <?php if( have_rows('videos') ): while ( have_rows('videos') ) : the_row(); ?>
       
      <a href="<?php the_sub_field('video_url')?>" class="club-single__video glightbox">
        <span class="club-single__poster">
          <?php $image = get_sub_field('video_poster'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
          <svg width="55.515" height="55.515"><use xlink:href="#video"></use></svg>
        </span>
        <h4><?php the_sub_field('info')?></h4>
      </a>
       
      <?php endwhile; endif; ?>
    </div>

    <div class="center">
      <a href="#" class="btn btn--blue"><?php _e('Inscrie-te la acest club','yass') ?></a>
    </div>

  </div>
</section>

<section class="cluburi2">
  <div class="container">
    <div class="main-title">
        <h2 class="title title--green"><?php the_field('title_mcc','option')?></h2>
        <p><?php the_field('info_mcc','option')?></p>

    </div>
    <div class="cluburi__grid">
    <?php  $args = array(
            'post_type' => 'club',
            'posts_per_page'=> 4,
            'orderby'=> 'date',
            'order' => 'DESC',
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','club') ?>
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
    </div>

    <div class="center">
      <a href="<?php echo get_the_permalink($archive)?>" class="btn btn--orange"><?php _e('Inapoi la cluburi','yass') ?></a>
    </div>

  </div>
</section>
<?php get_footer(); ?> 