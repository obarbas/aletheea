<?php get_header(); ?>	
<?php $archive = get_field('page_archive_gallery','option'); ?>

<section class="page-header">
  <?php $image = get_field('image_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_ph',$archive)?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section><?php get_template_part('templates/content','sections-viata') ?>

<section class="galerii">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle_gallery','option')?></span>
      <h2 class="title "><?php the_field('title_gallery','option')?></h2>
      <p><?php the_field('info_gallery','option')?></p>
    </div>

    <div class="small-gallery">
      <?php $images = get_field('gallery');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
      if( $images ): ?>
         <?php foreach( $images as $image ): ?>
          <a class="glightbox" href="<?php echo wp_get_attachment_url($image); ?>">
          <?php echo wp_get_attachment_image( $image, $size,'', array('class'=>'img-abs') ); ?>
          </a>
         <?php endforeach; ?>
      <?php endif; ?>
    </div>

    <div class="center">
      <a href="<?php echo get_the_permalink($archive)?>" class="btn btn--magenta"><?php _e('Inapoi la albume','yass') ?></a>
    </div>
      
  </div>
</section>


<?php get_footer(); ?> 