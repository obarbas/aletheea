<?php get_header(); ?>	
<?php $archive = get_field('page_archive_partneri','option'); ?>

<section class="page-header">
  <?php $image = get_field('image_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_ph',$archive)?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>
<?php get_template_part('templates/content','sections-curriculum') ?>


<section class="partener-single">
  <div class="container">

    <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle')?></span>
        <h2 class="title title--green"><?php the_field('title')?></h2>
        <p><?php the_field('info')?></p>
    </div>

    <div class="story__banner"><?php $image = get_field('banner'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?></div>

    <div class="col2__grid">
        <div class="col2__col col2__col--1">
          <?php the_post_thumbnail('large') ?>
          <h4><?php the_field('title_c')?></h4>
        </div>
        <div class="col2__col col2__col--2">
          <?php the_field('info_C')?>
          <a href="<?php echo get_the_permalink($archive)?>" class="btn btn--green"><?php _e('Inapoi la parteneri','yass') ?></a>
        </div>
    </div>

  </div>

</section>


<?php get_footer(); ?> 