<?php get_header(); ?>	
<?php $archive = get_field('page_archive_profesori','option'); ?>
<section class="page-header">
  <?php $image = get_field('image_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_ph',$archive); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_ph',$archive)?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>

<?php get_template_part('templates/content','sections-about') ?>


<section class="profesor">

  <div class="container">

    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle',$archive)?></span>
      <h2 class="title title--orange"><?php the_field('title',$archive)?></h2>
      <?php the_field('info', $archive)?>
    </div>

    <div class="story__grid">
        <div class="story__img">
        <?php the_post_thumbnail('large') ?>
          <h4><?php the_title() ?></h4>
          <p><?php the_field('subtitle')?></p>
        </div>
        <div class="story__info">
          <?php the_field('info')?>  
          <a href="<?php echo get_the_permalink($archive)?>" class="btn btn--orange"><?php _e('Inapoi la profesori','yass') ?></a>
        </div>
    </div>


  </div>


</section>
 
<?php get_footer(); ?> 