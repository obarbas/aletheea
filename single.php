<?php /*Template Name: Homepage*/ ?>
<?php get_header(); ?>	
<section class="page-header">
  <?php $image = get_field('image_phb', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_phb', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_phb', 'option')?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section>
<?php get_template_part('templates/content','sections-viata') ?>

<section class="single-article">

  <div class="container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="main-title">
            <span class="subtitle">BLOG</span>
            <h2 class="title title--magenta"><?php the_title(); ?></h2>
          </div>

          <div class="the-content">
            <?php the_content(); ?>
          </div>
          <div class="author center">
            <h4> <?php _e(' Articol scris de','yass') ?>: <?php the_author() ?></h4>
          </div>
          <?php get_template_part('templates/content','share') ?>
      <?php endwhile; endif; ?>
  </div>


</section>
<section class="articles">
  <div class="container">
      <div class="main-title">
        <span class="subtitle"><?php the_field('subtitle_mb', 'option')?></span>
        <h2 class="title title--green"><?php the_field('title_mb', 'option')?></h2>
      </div>

      <div class="articles__grid">
      <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page'=> 3,
            'orderby'=> 'date',
            'order' => 'DESC',
        
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) : ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php get_template_part('templates/content','post') ?> 
                <?php endwhile;?>
        <?php else: endif; wp_reset_postdata(); ?>
        
          
      </div>
      <div class="center">
        <a href="<?php echo get_the_permalink(get_option('page_for_posts'))?>" class="btn btn--purple"><?php _e('Vezi mai multe noutati','yass') ?></a>
      </div>
  </div>
</section>
 
<?php get_footer(); ?> 