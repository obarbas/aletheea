import Swiper from 'swiper/bundle';
import Glightbox from 'glightbox';

jQuery(document).ready(function($){


  const lightbox = Glightbox();

  var hero = new Swiper('.hero__slider', {
    loop: true,
    speed: 500,
    autoplay: {
      delay: 5000,
    },
    pagination: {
      el: '.hero__pagination',
      clickable: true,
      renderBullet: function (index,current) {
        var title = $('.hero__slide').eq(index+1).attr('data-title')
        return '<div class="hero__bullet '+ current +'"><span class="hero__bullet__title">' + title + '</span><span class="progress"><span class="progress--fill"></span></span></div>';
      },
    },
    
  });

  var banner = new Swiper('.banner__slider', {
    loop: true,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  })

  var prezetare = new Swiper('.prezentare__slider', {
    loop: true,
    centeredSlides: true,
    spaceBetween: 40,
    slidesPerView: 'auto',
    autoplay: {
      delay: 5000,
    },
    
  })

  var practices = new Swiper('.practices__gallery', {
    loop: true,
    pagination: {
      el: '.swiper-pagination',
    },
  })

 
  $('.burger').on('click', function(){
    $('body').toggleClass('show-menu');
    $('.overlay-menu').fadeToggle();
  })


  $('.overlay-menu__menu .menu-item-has-children > a ').on('click', function(){

    let parent = $(this).parents('.menu-item-has-children');

    if( $(this).parents('.menu-item-has-children').hasClass('active') ){
      parent.removeClass('active');
    }else{
      parent.addClass('active').siblings().removeClass('active');
    }

  })


  $(".register-toggle").on("click", function (e) {
    e.preventDefault();
    $("#customer_login").toggleClass("toggle-the-logins");
  });



})
