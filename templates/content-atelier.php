<div class="ateliere__post">
  <a href="<?php the_permalink()?>">
    <span class="ateliere__img">
      <?php the_post_thumbnail('large',array('class'=>'img-abs')) ?>
    </span>
    <span>Ateliere Aletheea</span>
    <h4><?php the_title() ?></h4>
  </a>
</div>