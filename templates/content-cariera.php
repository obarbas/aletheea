<div class="cariere__post">
  <a href="<?php the_permalink()?>">
    <span class="cariere__img">
      <?php the_post_thumbnail('large',array('class'=>'img-abs')) ?>
    </span>
    <h4><?php the_title() ?></h4>
  </a>
</div>