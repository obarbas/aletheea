<a href="<?php the_permalink()?>" <?php post_class() ?>>
  <?php the_post_thumbnail('large') ?>
  <span style="color: <?php the_field('color')?>"><?php echo get_the_terms( get_the_id(), 'club-category' )[0]->name ?></span>
  <h3><?php the_title() ?></h3>
</a>