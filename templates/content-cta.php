<section class="contact__cta">
    <div class="container">
        <?php if( have_rows('ctas','option') ): while ( have_rows('ctas','option') ) : the_row(); ?>
             
            <div class="contact__cta__link">
              <?php $image = get_sub_field('icon'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
              <h4><?php the_sub_field('title')?></h4>
              <p><?php the_sub_field('info')?></p>
              <a href="<?php echo get_sub_field('button')['url']?>" class="btn btn--orange"><?php echo get_sub_field('button')['label']?></a>
          </div>
             
        <?php endwhile; endif; ?>
         
    </div>
</section>
