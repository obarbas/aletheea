<div class="overlay-menu">
  <?php $image = get_field('background_image','option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>  
  <div class="container">
      <a href="<?php echo site_url()?>" class="overlay-menu__logo"><img  src='<?php echo get_template_directory_uri()?>/assets/images/aletheea.svg'></a>
        <?php
              wp_nav_menu( array(
                  'theme_location' => 'main',
                  'link_before'    => '<span>',
                  'link_after' => '</span>',
                  'container'=> false,
                  'walker'         => new Add_button_of_Sublevel_Walker, 
                  'menu_class' => 'overlay-menu__menu'
              ) );
          ?>

      <div class="overlay-menu__terms">
      <?php
              wp_nav_menu( array(
                  'theme_location' => 'terms',
                  'link_before'    => '<span>',
                  'link_after' => '</span>',
                  'container'=> false,
              ) );
          ?>
      </div>          
  </div>
</div>