<section class="partners">
  <div class="container">
    <div class="main-title">
      <span class="subtitle"><?php the_field('subtitle_p','option')?></span>
      <h2 class="title"><?php the_field('title_p','option')?></span>
    </div>
    <div class="partners__list">
      <?php if( have_rows('partners','option') ): while ( have_rows('partners','option') ) : the_row(); ?>
       
        <span><?php $image = get_sub_field('logo'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?></span>
       
      <?php endwhile; endif; ?>  
    </div>
  </div>
</section>
