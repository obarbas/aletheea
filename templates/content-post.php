<article class="article">
  <a href="<?php the_permalink()?>" class="article__link">
    <div class="article__img">
      <?php the_post_thumbnail('large',array('class'=>'img-abs')) ?>
    </div>
    <span class="date"><?php the_date() ?></span>
    <h3><?php the_title() ?></h3>
  </a>
</article>