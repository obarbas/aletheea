<div class="profesori__post">
  <a href="<?php the_permalink()?>">
    <span class="profesori__img">
    <?php the_post_thumbnail('large',array('class'=>'img-abs')) ?>
    </span>
    <h4><?php the_title() ?></h4>
    <p><?php the_field('subtitle')?></p>
  </a>
</div>