<div class="share">
  <h3>Share this article:</h3>
  <ul>
    <li><button data-sharer="facebook" data-hashtag="aletheea" data-url="<?php the_permalink()?>"><svg width="11.615" height="22"><use xlink:href="#facebook2"></use></svg></button></li>
    <li><button data-sharer="linkedin" data-url="<?php the_permalink()?>"><svg width="20.219" height="20.228"><use xlink:href="#linkedin"></use></svg></button></li>
    <li><button data-sharer="twitter" data-title="<?php the_title()?>" data-hashtags="aletheea" data-url="<?php the_permalink()?>"><svg width="20.599" height="16.741"><use xlink:href="#twitter"></use></svg></button></li>
  </ul>
</div>