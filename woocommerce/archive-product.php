<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );?>
<section class="page-header">
  <?php $image = get_field('image_shop', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'img-abs') ); }?>
  <div class="container">
      <h2 class="title">
        <?php $image = get_field('icon_shop', 'option'); if( $image ) { echo wp_get_attachment_image( $image, 'full','',array('class'=>'') ); }?>
        <span><?php the_field('title_shop', 'option')?></span>
      </h2>
  </div>
  <img class="hero__scroll"  src='<?php echo get_template_directory_uri()?>/assets/images/scroll.svg'>
</section> 

<?php get_template_part('templates/content','sections') ?>


<div class="container">

		<div class="main-title">
			<span class="subtitle"><?php the_field('subtitle_tshop', 'option')?></span>
      <h2 class="title title--blue"><?php the_field('title_tshop', 'option')?></h2>	
			<p><?php the_field('info_tshop', 'option')?></p>
	  </div>

<?php

do_action( 'woocommerce_before_main_content' );

if ( woocommerce_product_loop() ) {

	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();


	do_action( 'woocommerce_after_shop_loop' );
} else {
	
	do_action( 'woocommerce_no_products_found' );
}


do_action( 'woocommerce_after_main_content' ); ?>


</div>


<?php get_footer( 'shop' );
